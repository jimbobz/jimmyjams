#' Convenience function to run BQ queries
#' 
#' @param query Query string to execute
#' @param project GCP project to use, defaults to "king-candy-prod"
#' 
#' @return Data frame of results
#' 
#' @export
#' @importFrom bigrquery bq_project_query bq_table_download

bq <- function(query, project = "king-candy-prod") {
  res <- project %>% 
    bq_project_query(query) %>% 
    bq_table_download()
  
  return(res)
}